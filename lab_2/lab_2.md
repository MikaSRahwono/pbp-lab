1. Apakah perbedaan JSON dan XML?
   JSON dan XML merupakan bentuk atau format untuk melakukan penyimpanan data. JSON dan XML mempunyai perbedaan, kelebihan, dan kekurangannya masing-masing diantaranya:

JSON

- Merupakan format yang lebih ringan untuk melakukan penukaran data
- Berdasarkan atas bahasa javascript
- Lebih mudah dibaca dan dipahami
- Format penyimpanan berbentuk dictionary dimana terdapat key dan value
- JSON merupakan notasi objek JavaScript

XML

- XML lebih digunakan untuk membawa data, bukan untuk menampilkan data
- Berasal dari SGML
- Lebih rumit untuk dibaca dan dipahami
- Format penyimpanan berbentuk tree
- XML merupakan bahasa markup

2. Apakah perbedaan antara HTML dan XML?
   HTML dan XML sama-sama merupakan sebuah markup language tetapi keduanya biasanya difungsikan untuk hal yang berbeda. Dimana HTML berisi struktur website untuk menampilkan informasi, sedangkan XML berisi struktur untuk penyimpanan data dan pengiriman data. HTML digunakan hanya untuk menampilkan data tidak bisa untuk membawa/menyimpan data, dimana XML digunakan untuk membawa data dari database satu ke database yang lain.

Berdasarkan tags untuk menggunakan markup languagenya juga berbeda. HTML memiliki tags yang sudah ditentukan, seperti paragraf(<p>), header(<h1>), dll. Sedangkan tags pada XML ditentukan sendiri semau usernya. Perbedaan lainnya yaitu HTML merupakan sebuah static files dimana XML dinamis dan HTML tidak case sensitive dimana XML case sensitif, dsb.

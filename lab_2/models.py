from django.db import models

# Create your models here.


class Note(models.Model):
    to_note = models.CharField(max_length=32)
    from_note = models.CharField(max_length=32)
    title = models.CharField(max_length=64)
    message = models.CharField(max_length=512)

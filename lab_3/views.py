from django.http import response
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)


def add_friend(request):
    form = FriendForm(request.POST or None)
    response = {'form': form}
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-3')
    return render(request, 'lab3_form.html', response)

from django.db.models import fields
from lab_1.models import Friend
from django import forms


class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'

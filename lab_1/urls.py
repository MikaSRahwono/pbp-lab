from django.urls import path
from .views import index, friend_list
from django.urls import path

urlpatterns = [
    path('', index, name='index'),
    path('friends', friend_list, name='friend_list')
    # TODO Add friends path using friend_list Views
]
